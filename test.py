import unittest
import requests

from backend import MemoryBackend

class TestMemoryBackend(unittest.TestCase):
    def setUp(self):
        self.backend = MemoryBackend()

    def tearDown(self):
        self.backend.purge()

    def test_set(self):
        self.backend.set("foo", "bar")
        self.assertEqual(self.backend.transaction["foo"], "bar")

    def test_set_already_exists(self):
        self.backend.set("foo", "bar")        
        with self.assertRaises(KeyError):
            self.backend.set("foo", "bar")

    def test_update_missing(self):
        with self.assertRaises(KeyError):
            self.backend.update("foo", "bar")
            
    def test_update(self):
        self.backend.data["foo"] = "bar"
        self.backend.update("foo", "bar")
        self.assertEqual(self.backend.transaction["foo"], "bar")
        
    def test_get(self):
        self.backend.data["foo"] = "bar"
        self.assertEqual(self.backend.get("foo"), "bar")

    def test_get_missing(self):
        with self.assertRaises(KeyError):
            self.backend.get("foo")
            
    def test_get_from_transaction(self):
        self.backend.transaction["foo"] = "bar"
        with self.assertRaises(KeyError):
            self.backend.get("foo")
            
    def test_commit(self):
        self.backend.transaction["foo"] = "bar"
        self.backend.commit()
        self.assertEqual(self.backend.get("foo"), "bar")
        
    def test_delete_then_get(self):
        self.backend.data["foo"] = "bar"
        self.backend.delete("foo")
        self.backend.commit()
        with self.assertRaises(KeyError):
            self.backend.get("foo")
        
    def test_delete_from_transaction_then_get(self):
        self.backend.transaction["foo"] = "bar"
        self.backend.delete("foo")
        with self.assertRaises(KeyError):
            self.backend.get("foo")

    def test_set_then_rollback(self):
        self.backend.set("foo", "bar")
        self.backend.set("thing", "value")
        
        self.assertEqual(self.backend.get_from_transaction("foo"), "bar")
        self.assertEqual(self.backend.get_from_transaction("thing"), "value")
        
        self.backend.rollback()

        with self.assertRaises(KeyError):
            self.backend.get("foo")        


BASE_URL = 'http://localhost:4000'

class TestDatabaseServer(unittest.TestCase):
    """
    This is an integration test that will make actual requests
    against the locally running server. I would much rather invoke the handler
    directly, but in the interest of time, I'm not doing that now.
    Another change I'd make here if I had time, 
    is to add in/out files as parameters to the database class, 
    so that we could pass temporary/in-memory "files" in this test, 
    inspect the output, and mock the input (rather than having calls 
    dependent on one another).
    """

    def test_post(self):
        response = requests.post(
            "{}/set".format(BASE_URL),
            json={'foo': 'bar'})
        self.assertIn(response.status_code, (200, 201))  # or 201
                
    def test_post_multiple(self):
        response = requests.post(
            "{}/set".format(BASE_URL),
            json={'foo': 'bar', 'bar': 'foo'})
        self.assertEqual(response.status_code, 400)
        
    def test_get(self):
        requests.post("{}/set".format(BASE_URL), json={'foo': 'bar'})
        response = requests.get("{}/foo".format(BASE_URL))
        self.assertEqual(response.status_code, 200)

    def test_commit(self):
        requests.post("{}/set".format(BASE_URL), json={'foo': 'bar'})
        response = requests.post("{}/commit".format(BASE_URL))        
        self.assertEqual(response.status_code, 204)

    def test_delete(self):
        requests.post("{}/set".format(BASE_URL), json={'foo': 'bar'})
        response = requests.delete("{}/set".format(BASE_URL), json='foo')
        self.assertEqual(response.status_code, 200)

    def test_delete_bad_path(self):
        requests.post("{}/set".format(BASE_URL), json={'foo': 'bar'})
        response = requests.delete("{}/xxx".format(BASE_URL), json='foo')
        self.assertEqual(response.status_code, 404)

    def test_post_new(self):
        requests.delete("{}/set".format(BASE_URL), json='foo')
        response = requests.post(
            "{}/set".format(BASE_URL),
            json={'foo': 'bar'})
        self.assertEqual(response.status_code, 201)
    
    def test_get_missing(self):
        requests.delete("{}/set".format(BASE_URL), json='foo')
        requests.post("{}/commit".format(BASE_URL))
        response = requests.get("{}/foo".format(BASE_URL))
        self.assertEqual(response.status_code, 404)
