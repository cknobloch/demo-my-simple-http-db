from abc import ABCMeta, abstractmethod

import json
import os

DEFAULT_DATABASE_FILENAME = "db.json"

class DatabaseBackend(object):
    __metaclass__ = ABCMeta

    @abstractmethod
    def get(self, key):
        """
        Returns the value that is stored at the key. 
        If the value does not exist, raises a KeyError.
        """
        pass

    @abstractmethod    
    def set(self, key, value):
        """
        Stores `value` with the associated `key`.
        If the value already exists, raises a KeyError.
        """
        pass

    @abstractmethod    
    def update(self, key, value):
        """
        Updates `value` with the associated `key`.
        If the value does not already exist, raises a KeyError.
        """
        pass

    @abstractmethod    
    def delete(self, key):
        """
        Deletes the entry for `key`. 
        If the key does not exist, returns a KeyError.
        """
        pass

    @abstractmethod
    def commit(self):
        """
        Persists updates made in the current transaction.
        """
        pass

    @abstractmethod    
    def rollback(self):
        """
        Throws away updates made in the current transaction and 
        start from the last committed state.
        """
        pass

delete_sentinel = object()

class MemoryBackend(DatabaseBackend):
    def __init__(self):
        self.data = {}
        self.transaction = {}

    def get(self, key):
        return self.data[key]        
                
    def get_from_transaction(self, key):
        try:
            value = self.transaction[key]
        except KeyError:
            value = self.data[key]
        else:
            if value is delete_sentinel:
                raise KeyError(key)
        return value

    def set(self, key, value):
        try:
            self.get_from_transaction(key)
        except KeyError:
            self.transaction[key] = value
        else:
            raise KeyError(key)
        
    def update(self, key, value):
        try:
            self.get_from_transaction(key)
        except:
            raise
        else:
            self.transaction[key] = value

    def delete(self, key):
        self.transaction[key] = delete_sentinel

    def commit(self):
        for k, v in self.transaction.iteritems():
            if v is delete_sentinel:
                del self.data[k]
            else:
                self.data[k] = v
                
        self.transaction = {}

    def rollback(self):
        self.transaction = {}

    def purge(self):
        """Delete all persisted data."""
        
        self.rollback()
        
        for k in list(self.data):
            self.delete(k)
            
        self.commit()

class FileBackend(MemoryBackend):
    """
    The file backend stores transaction data in memory and 
    persists data to a file on commit.
    """
    
    def __init__(self, filename=DEFAULT_DATABASE_FILENAME):
        super(FileBackend, self).__init__()
        
        self.filename = filename

        self.load_from_file()

    def load_from_file(self):
        if not os.path.exists(self.filename):
            return
        
        with open(self.filename, 'rt') as f:
            self.data = json.loads(f.read())
        
    def commit(self):
        super(FileBackend, self).commit()

        with open(self.filename, 'wt') as f:
            f.write(json.dumps(self.data))
