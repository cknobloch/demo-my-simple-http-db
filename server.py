import json
import re
import SocketServer
from SimpleHTTPServer import SimpleHTTPRequestHandler

from backend import FileBackend

db_backend = FileBackend()

class DatabaseRequestHandler(SimpleHTTPRequestHandler):        
    def respond_with_message(self, status_code, message=""):
        self.send_response(status_code, message)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()

    def respond_with_not_found_error(self):
        self.respond_with_message(404)

    def respond_with_success(self, message=""):
        self.respond_with_message(200, message)

    def respond_with_json(self, data, status_code=200):
        self.send_response(status_code)
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(data))

    def json_body(self):
        content_length = int(self.headers.getheader('content-length', 0))
        return json.loads(self.rfile.read(content_length))
            
    def do_GET(self):
        m = re.match(r'/(?P<key>\w+)$', self.path)
        if m:
            key = m.group('key')

            try:
                value = db_backend.get(key)
                self.respond_with_json({key: value})
            except KeyError:
                self.respond_with_not_found_error()

            return
            
        self.respond_with_not_found_error()

    def do_POST(self):
        m = re.match(r'/set$', self.path)
        if m:
            data = self.json_body()
            if len(data) != 1:
                self.respond_with_message(400)
                return
            
            key, value = next(data.iteritems())
            
            try:
                db_backend.set(key, value)
                self.respond_with_message(201)
            except KeyError:
                db_backend.update(key, value)
                self.respond_with_success()
                
            return

        m = re.match(r'/commit$', self.path)
        if m:
            db_backend.commit()
            self.respond_with_message(204)
            return
            
        self.respond_with_not_found_error()            

    def do_DELETE(self):
        m = re.match(r'/set$', self.path)
        if m:
            try:
                db_backend.delete(self.json_body())
            except KeyError:
                pass
            
            self.respond_with_success()

            return

        self.respond_with_not_found_error()        

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser(
        description='Run the database server')
    
    parser.add_argument('--port', type=int, default=4000)
    
    args = parser.parse_args()

    httpd = SocketServer.TCPServer(("", args.port), DatabaseRequestHandler)
    
    print "Serving database at localhost:{}.".format(args.port)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        db_backend.commit()
