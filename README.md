# README #

## Overview ##

This is a demo of a simple transactional HTTP database that I wrote in a couple hours.

The only external dependency is the `requests` package (along with its dependencies), that are required to run the test suite. I have assumed that the test environment comes with Python 2.7.9 or greater (but < 3.0), so `sudo pip install` will work.

## Project layout ##

The database is made up of three Python modules: server, backend, and test.

### server ###

I implemented a barebones HTTP request handler that calls the database backend (I consider the whole package to be the "database").

The HTTP request handler would quickly need some re-architecting if it were going to be developed any further. If the scope of this demo were much larger, I would have used Flask.

### backend ###

The core database functionality is spread over three classes: DatabaseBackend, MemoryBackend, and FileBackend.

DatabaseBackend is an abstract base class representing a database backend.

MemoryBackend is used for testing and is extended by the FileBackend. All committed and transaction data is stored in the class instance.

FileBackend persists data to a file in JSON format, when the commit method is called. It loads data from the same file when the class is instantiated. Note that reads from the backend still come from memory -- this would not work if multiple "servers" were sharing the same file.

The approach I took to handling transactions, is to keep only the current state around (in memory, as a member variable). There is no ledger of the transactions that occur, only this current state.

Deletes are handled by using a "delete sentinel" for simplicity, so that all the transaction data can be kept in one structure.

### test ###

I wrote two test suites.

The first is a unit test that exercises the MemoryBackend directly by mocking data.

The second is an integration test that will make actual requests against a locally running server. I would much rather invoke the handler or service (if I had abstracted this) directly, and make this a unit test, but in the interest of timeI decided not to.